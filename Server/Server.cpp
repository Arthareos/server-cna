#include <iostream>

#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>

#include "GreetingServiceImpl.h"
#include "HoroscopeServiceImpl.h"

int main()
{
    std::string serverAddress("localhost:8888");

    grpc_impl::ServerBuilder serverBuilder;
    serverBuilder.AddListeningPort(serverAddress, grpc::InsecureServerCredentials());

    CGreetingServiceImpl greetService;
    serverBuilder.RegisterService(&greetService);

    CHoroscopeServiceImpl horoscopeService;
    serverBuilder.RegisterService(&horoscopeService);

    std::unique_ptr<grpc_impl::Server> server(serverBuilder.BuildAndStart());
    std::cout << "Server listening on: " << serverAddress << std::endl;
    server->Wait();
}