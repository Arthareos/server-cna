#pragma once

#include <fstream>
#include <string>
#include <iomanip>

#include <HoroscopeService.grpc.pb.h>
#include <Utils.h>

class CHoroscopeServiceImpl final : public HoroscopeService::Service {
public:
	CHoroscopeServiceImpl() {}

	::grpc::Status CalculateSign(grpc::ServerContext* context, const ::HoroscopeRequest* request, ::HoroscopeResponse* response) override;
};