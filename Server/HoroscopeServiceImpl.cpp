#include "HoroscopeServiceImpl.h"

::grpc::Status CHoroscopeServiceImpl::CalculateSign(grpc::ServerContext* context, const::HoroscopeRequest* request, ::HoroscopeResponse* response)
{
	Utils::CDate date(request->date());
	std::vector<Utils::CHoroscopeSign> horoscopeSigns;

	std::ifstream inputSigns("HoroscopeSigns.txt");
	
	while (!inputSigns.eof())
	{
		std::string name;
		std::getline(inputSigns, name);

		std::string auxDate;
		std::getline(inputSigns, auxDate);
		Utils::CDate start(auxDate);

		std::getline(inputSigns, auxDate);
		Utils::CDate end(auxDate);

		Utils::CHoroscopeSign sign;
		sign.SetName(name);
		sign.SetStartDay(start);
		sign.SetEndDay(end);

		horoscopeSigns.push_back(sign);
	}

	for (int index = 0; index < horoscopeSigns.size() - 1; index++)
	{
		if (horoscopeSigns.at(index).GetEndDate().GetMonth() == date.GetMonth())
		{
			if (date.GetDay() > horoscopeSigns.at(index).GetEndDate().GetDay())
			{
				if (index == horoscopeSigns.size() - 1)
					index = 0;
				else
					index++;
			}

			std::cout << "\"CHoroscopeServiceImpl::CalculateSign\" method called: " << date << ", the sign is: " << horoscopeSigns.at(index).GetName() << ";" << std::endl;
			response->set_sign(horoscopeSigns.at(index).GetName());
			return grpc::Status::OK;
		}
	}
}