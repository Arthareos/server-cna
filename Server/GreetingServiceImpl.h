#pragma once

#include <GreetingService.grpc.pb.h>

class CGreetingServiceImpl final : public GreetingService::Service {
public:
	CGreetingServiceImpl() {}

	grpc::Status Greet(grpc::ServerContext* context, const GreetingRequest* request, EmptyResponse* response) override;
};