#include "GreetingServiceImpl.h"

#include <string>
#include <iostream>

grpc::Status CGreetingServiceImpl::Greet(grpc::ServerContext* context, const GreetingRequest* request, EmptyResponse* response)
{
	std::cout << "\"CGreetingServiceImpl::Greet\" method called:  Hello, " + request->name() << ";" << std::endl;
	return grpc::Status::OK;
}
