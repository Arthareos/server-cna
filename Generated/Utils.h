#pragma once

#include <string>
#include <iostream>
#include <sstream>
#include <cmath>

class Utils {
public:
	class CDate {
	private:
		int m_day;
		int m_month;
		int m_year;

	public:
		CDate();
		CDate(std::string date);
		~CDate() {}

		const int& GetDay() const { return m_day; }
		const int& GetMonth() const { return m_month; }
		const int& GetYear() const { return m_year; }

		void SetDate(std::string date);
		void SetDay(int16_t day) { this->m_day = day; }
		void SetMonth(int16_t month) { this->m_month = month; }
		void SetYear(int16_t year) { this->m_year = year; }

		friend std::ostream& operator<<(std::ostream& output, CDate date);
		const std::string& ToString();

		static bool DateValidator(const std::string& date);
	};

	class CHoroscopeSign {
	private:
		std::string m_name;
		CDate m_startDate;
		CDate m_endDate;

	public:
		CHoroscopeSign() { m_name = "Undefined"; }
		CHoroscopeSign(std::string name, std::string start, std::string end);
		~CHoroscopeSign() {};

		const std::string& GetName() const { return m_name; }
		const CDate& GetStartDate() const { return m_startDate; }
		const CDate& GetEndDate() const { return m_endDate; }

		void SetName(std::string name) { m_name = name; }
		void SetStartDay(CDate date) { m_startDate = date; }
		void SetEndDay(CDate date) { m_endDate = date; }
	};
};

