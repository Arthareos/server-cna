#include "Utils.h"

Utils::CDate::CDate()
{
	m_month = 1;
	m_day = 1;
	m_year = 1970;
}

Utils::CDate::CDate(std::string date)
{
	if (date.length() == 5)
	{
		m_month = std::stoi(date.substr(0, 2));
		m_day = std::stoi(date.substr(3, 2));
		m_year = 1200;
		return;
	}

	if (DateValidator(date))
	{
		m_month = std::stoi(date.substr(0, 2));
		m_day = std::stoi(date.substr(3, 2));
		m_year = std::stoi(date.substr(6, 4));
		return;
	}
	
	m_month = 1;
	m_day = 1;
	m_year = 1970;
}

void Utils::CDate::SetDate(std::string date)
{
	Utils::CDate aux(date);

	this->m_year = aux.GetYear();
	this->m_month = aux.GetMonth();
	this->m_day = aux.GetDay();
}

std::ostream& operator<<(std::ostream& output, Utils::CDate date)
{
	output << date.GetMonth() << "/" << date.GetDay() << "/" << date.GetYear();
	return output;
}

const std::string& Utils::CDate::ToString()
{
	std::string aux;

	//Y U NO WORK?!?!!
	//aux = m_month + "/" + m_day + "/" + m_year;

	//Y U WORK?
	aux = m_month;
	aux += "/";
	aux += m_day;
	aux += "/";
	aux += m_year;

	return aux;
}

bool Utils::CDate::DateValidator(const std::string& date)
{
	int16_t day = 1;
	int16_t month = 1;
	int16_t year = 1200;

	if (date.length() == 10)
	{
		month = std::stoi(date.substr(0, 2));
		day = std::stoi(date.substr(3, 2));
		year = std::stoi(date.substr(6, 4));
	}

	if (date.length() == 5)
	{
		month = std::stoi(date.substr(0, 2));
		day = std::stoi(date.substr(3, 2));
	}

	if (date.length() != 10 && date.length() != 5)
	{
		return false;
	}

	if (month > 12)
		return false;

	switch (month)
	{
	case 1:  //January
	case 3:  //March
	case 5:  //May
	case 7:  //July
	case 8:  //August
	case 10: //October
	case 12: //December
		if (day > 31)
			return false;
		return true;

	case 4:  //April
	case 6:  //June
	case 9:  //September
	case 11: //November
		if (day > 30)
			return false;
		return true;

	case 2:  //February
		if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0))
		{
			if (day > 29)
				return false;
			return true;
		}
		if (day > 28)
			return false;
		return true;

	default:
		return false;
	}

	return true;
}

Utils::CHoroscopeSign::CHoroscopeSign(std::string name, std::string start, std::string end)
{
	m_name = name;
	m_startDate = start;
	m_endDate = end;
}
