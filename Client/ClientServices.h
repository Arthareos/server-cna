#pragma once

#include <iostream>

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

#include <GreetingService.grpc.pb.h>
#include <HoroscopeService.grpc.pb.h>
#include <Utils.h>

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

constexpr auto ADDRESS = "localhost:8888";

class CClientServices {
public:
	static void GreetingService();
	static void HoroscopeService();
};