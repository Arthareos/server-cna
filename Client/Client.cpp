#include <iostream>

#include <grpc/grpc.h>
#include <grpcpp/channel.h>
#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/security/credentials.h>

#include <GreetingService.grpc.pb.h>
#include <HoroscopeService.grpc.pb.h>

#include "ClientServices.h"

using grpc::Channel;
using grpc::ClientContext;
using grpc::ClientReader;
using grpc::ClientReaderWriter;
using grpc::ClientWriter;

void exec()
{
	bool isExiting = false;
	int16_t choice;

	while (!isExiting)
	{
		std::cout << " 1> Saluta serverul;" << std::endl;
		std::cout << " 2> Afla semnul tau zodiacal;" << std::endl;

		std::cout << "  +> Selectati operatia dorita: ";
		std::cin >> choice;

		switch (choice)
		{
		case 1:
			CClientServices::GreetingService();
			break;

		case 2:
			CClientServices::HoroscopeService();
			break;

		default:
			break;
		}

		//de editat daca nu uit, daca uit, c'est la vie :/
		isExiting = true;
	}
}

int main()
{
	grpc_init();

	exec();

	grpc_shutdown();
	return 0;
}