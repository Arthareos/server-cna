#include "ClientServices.h"

void CClientServices::GreetingService()
{
	ClientContext context;

	auto stub = GreetingService::NewStub(grpc::CreateChannel(ADDRESS, grpc::InsecureChannelCredentials()));

	GreetingRequest request;
	EmptyResponse response;

	std::string aux;
	std::cout << " +> Introduceti numele dvs.: ";
	std::cin >> aux;
	request.set_name(aux);

	grpc::Status status = stub->Greet(&context, request, &response);

	if (status.ok())
	{
		std::cout << "The server received the message!" << std::endl;
	}
}

void CClientServices::HoroscopeService()
{
	ClientContext context;

	auto stub = HoroscopeService::NewStub(grpc::CreateChannel(ADDRESS, grpc::InsecureChannelCredentials()));

	HoroscopeRequest request;
	HoroscopeResponse response;

	std::string date;
	std::cout << " +> Introduceti data dvs. de nastere in formatul (ll/zz/aaaa): ";
	std::cin >> date;
	request.set_date(date);

	while (!Utils::CDate::DateValidator(date))
	{
		system("cls");
		std::cout << " !> Data este invalida, introduceti alta data!" << std::endl;
		std::cout << "  +> Data in formatul (ll/zz/aaaa, ex \"12/20/2010\"): ";
		std::cin >> date;
	}

	grpc::Status status = stub->CalculateSign(&context, request, &response);

	if (status.ok())
	{
		std::cout << "Your sign is: " << response.sign() << std::endl;
	}
	else
	{
		std::cout << "Something went wrong server side, stay tuned!" << std::endl;
	}
}
